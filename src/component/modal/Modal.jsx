import React from 'react';
import modalStyle from'./Modal.module.css'

import jsPDF from 'jspdf';
import 'jspdf-autotable';

const Modal = (props) => {
    function generatePdf () {
        console.log("data", props.name, props.description, props.client, props.contractor, props.xMax, props.xMin, props.yMax, props.yMin, props.zMax, props.zMin);
        const doc = new jsPDF();
        const tableRows = [];
        const tableColumn = ['project Name', 'Project Description','Client','Contractor','X-Max','X-Min','Y-Max','Y-Min','Z-Max','Z-Min'];
        const tData = [
            props.name,
            props.description,
            props.client,
            props.contractor,
            props.xMax,
            props.xMin,
            props.yMax,
            props.yMin,
            props.zMax,
            props.zMin
        ];
        tableRows.push(tData);
        doc.autoTable(tableColumn, tableRows, {startY : 30 });
        doc.text('Report',14,15);
        doc.save('Result.pdf');
    }; 
    
    function close () {
        props.init();
    };
    return (
        <div className={modalStyle.container}>
            <div className={modalStyle.head}>
                <p className={modalStyle.headText}>Result</p>
            </div>
            <div className={modalStyle.tableContainer}>
                <table>
                    <tr>
                        <th>Project Name</th>
                        <th>Project Description</th>
                        <th>Client</th>
                        <th>Contractor</th>
                        <th>X - Max</th>
                        <th>X - Min</th>
                        <th>Y - Max</th>
                        <th>Y - Min</th>
                        <th>Z - Max</th>
                        <th>Z - Min</th>
                    </tr>
                    <tr>
                        <td>{props.name}</td>
                        <td>{props.description}</td>
                        <td>{props.client}</td>
                        <td>{props.contractor}</td>
                        <td>{props.xMax}</td>
                        <td>{props.xMin}</td>
                        <td>{props.yMax}</td>
                        <td>{props.yMin}</td>
                        <td>{props.zMax}</td>
                        <td>{props.zMin}</td>
                    </tr>
                </table>
            </div>
            <div className={modalStyle.btnContainer}>
                <button className={modalStyle.btn} onClick={close}>Close</button>
                <button className={modalStyle.btn} onClick={generatePdf}>Generate PDF</button>
            </div>
        </div>
    );
};

export default Modal;