import React, { useState } from 'react';
import mainStyle from './Main.module.css';

import First from './first/First';
import Second from './second/Second';
import Modal from '../modal/Modal'

const Main = () => {
    const [step, setStep] = useState(0);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [client, setClient] = useState('');
    const [contractor, setContractor] = useState('');
    const [xMax, setXMax] = useState('');
    const [xMin, setXMin] = useState('');
    const [yMax, setYMax] = useState('');
    const [yMin, setYMin] = useState('');
    const [zMax, setZMax] = useState('');
    const [zMin, setZMin] = useState('');


    function increment(type, info){
        if(type === 'first') {
            setName(info.name);
            setDescription(info.description);
            setClient(info.client);
            setContractor(info.contractor);
        }
        if(type === 'second') {
            setXMax(info.xMax);
            setXMin(info.xMin);
            setYMax(info.yMax);
            setYMin(info.yMin);
            setZMax(info.zMax);
            setZMin(info.zMin);
        }
        setStep(step + 1);
    };

    function init () {
        setStep(0);
        setName('');
        setDescription('');
        setClient('');
        setContractor('');
        setXMax('');
        setXMin('');
        setYMax('');
        setYMin('');
        setZMax('');
        setZMin('');
    };

    return (
        <div className={mainStyle.main}>
            {(step === 0) && <First increment={increment}/>}
            {(step === 1) && 
                <Second 
                    name={name}
                    description={description}
                    client={client}
                    contractor={contractor}
                    increment={increment}
                />
            }
            {(step === 2) && 
                <Modal
                    name={name}
                    description={description}
                    client={client}
                    contractor={contractor}
                    xMax={xMax}
                    xMin={xMin}
                    yMax={yMax}
                    yMin={yMin}
                    zMax={zMax}
                    zMin={zMin}
                    init={init}
                />
            }
        </div>
    );
};
export default Main;