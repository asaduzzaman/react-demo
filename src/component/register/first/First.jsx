import React, {useState} from 'react';

import firstStyle from './First.module.css';

const First = (props) => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [client, setClient] = useState('');
    const [contractor, setContractor] = useState('');
    const [errors, setErrors] = useState({
        name: '',
        description: '',
        client: '',
        contractor: ''
    });  
    const handlar = (e, type) => {
        let tempErrors = { ...errors };
  
          if(type === 'name') {
              tempErrors.name = '';
              let tempName = e.target.value;
              setName(tempName);
          }
          if(type === 'description') {
              tempErrors.description = '';
              let tempDescription = e.target.value;
              setDescription(tempDescription);
          }
          if(type === 'client') {
              tempErrors.client = '';
              let tempClient = e.target.value;
              setClient(tempClient);
          }
          if(type === 'contractor') {
            tempErrors.contractor = '';
            let tempContractor = e.target.value;
            setContractor(tempContractor);
        }
          setErrors(tempErrors);
      };

      const validate = () =>{
        let tempErrors = { ...errors };
        let valid = true;
        if(name === '') {
            valid = false;
            tempErrors.name = 'This field is required';
        }
        if(description === '') {
          valid = false;
          tempErrors.description = 'This field is required';
        }
        if(client === '') {
          valid = false;
          tempErrors.client = 'This field is required';
        }
        if(contractor === '') {
            valid = false;
            tempErrors.contractor = 'This field is required';
          }
        setErrors(tempErrors);
        if(valid) {
            props.increment('first',{name, description, client, contractor});
        }
    };

  
    return (
        <div className={firstStyle.container}>
            <div className={firstStyle.head}>
                <p className={firstStyle.headText}>ABC Company</p>
            </div>
            <div className={firstStyle.form}>
                <div className={firstStyle.inputWrapper}>
                    <label>Project Name</label>
                    <input 
                        placeholder="Name"
                        value={name}
                        onChange={(e) => handlar(e, 'name')}
                    />
                    {errors.name && (
                        <span className="error">{errors.name}</span>
                    )}
                </div>
                <div className={firstStyle.inputWrapper}>
                    <label>Project Description</label>
                    <textarea
                        rows="8" 
                        placeholder="Description"
                        value={description}
                        onChange={(e) => handlar(e, 'description')}
                    />
                    {errors.description && (
                        <span className="error">{errors.description}</span>
                    )}
                </div>
                <div className={firstStyle.inputWrapper}>
                    <label>Client</label>
                    <input 
                        placeholder="Client"
                        value={client}
                        onChange={(e) => handlar(e, 'client')}
                    />
                    {errors.client && (
                        <span className="error">{errors.client}</span>
                    )}

                </div>
                <div className={firstStyle.inputWrapper}>
                    <label>Contractor</label>
                    <input 
                        placeholder="Contractor"
                        value={contractor}
                        onChange={(e) => handlar(e, 'contractor')}
                    />
                    {errors.contractor && (
                        <span className="error">{errors.contractor}</span>
                    )}

                </div>
                <div>
                    <button className={firstStyle.btn} onClick={validate}>Next</button>
                </div>
            </div>
        </div>
    );
};

export default First;