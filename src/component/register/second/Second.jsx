import React, {useState} from 'react';
import { CSVReader } from 'react-papaparse';

import firstStyle from '../first/First.module.css';
import secondStyle from './Second.module.css';

const First = (props) => {
    const [uploadCsv, setUploadCsv] = useState(false);
    const [xMax, setXMax] = useState('');
    const [xMin, setXMin] = useState('');
    const [yMax, setYMax] = useState('');
    const [yMin, setYMin] = useState('');
    const [zMax, setZMax] = useState('');
    const [zMin, setZMin] = useState('');
    const [errors, setErrors] = useState({
        xMax: '',
        xMin: '',
        yMax: '',
        yMin: '',
        zMax: '',
        zMin: ''
    });
    const handleOnCsvDrop = (data) => {
        setUploadCsv(true);
        let i, x = [], y = [], z = [], kp = [];
        for (i = 0 ; i < data.length ; i++) {
            x.push(Object.values(data[i].data)[1]);
            y.push(Object.values(data[i].data)[2]);
            z.push(Object.values(data[i].data)[3]);
            kp.push(Object.values(data[i].data)[0]);
        }
        setXMax(arrayMax(x));
        setXMin(arrayMin(x));
        setYMax(arrayMax(y));
        setYMin(arrayMin(y));
        setZMax(arrayMax(z));
        setZMin(arrayMin(z));
    };
    const arrayMin = (arr) => {
        var len = arr.length, min = Infinity;
        while (len--) {
          if (arr[len] < min) {
            min = arr[len];
          }
        }
        return min;
    };

    const arrayMax = (arr) => {
        var len = arr.length, max = -Infinity;
        while (len--) {
          if (arr[len] > max) {

            max = arr[len];
          }
        }
        return max;
    };
    const handlar = (e, type) => {
        console.log("e", e);
        let tempErrors = { ...errors };
        if(type === 'xMax') {
            tempErrors.xMax = '';
            let tempXMax = e.target.value;
            setXMax(tempXMax);
        }
        if(type === 'xMin') {
            tempErrors.xMin = '';
            let tempXMin = e.target.value;
            setXMin(tempXMin);
        }
        if(type === 'yMax') {
            tempErrors.yMax = '';
            let tempYMax = e.target.value;
            setYMax(tempYMax);
        }
        if(type === 'yMin') {
            tempErrors.yMin = '';
            let tempYMin = e.target.value;
            setYMin(tempYMin);
        }
        if(type === 'zMax') {
            tempErrors.zMax = '';
            let tempZMax = e.target.value;
            setZMax(tempZMax);
        }
        if(type === 'zMin') {
            tempErrors.zMin = '';
            let tempZMin = e.target.value;
            setZMin(tempZMin);
        }          
        setErrors(tempErrors);
      };

      const validate = () =>{
        let tempErrors = { ...errors };
        let valid = true;
        if(xMax === '') {
            valid = false;
            tempErrors.xMax = 'This field is required';
        }
        if(xMax !== '' && xMax < 0) {
            valid = false;
            tempErrors.xMax = 'Should be positive';
        }
        if(yMax === '') {
            valid = false;
            tempErrors.yMax = 'This field is required';
        }
        if(yMax !== '' && yMax < 0) {
            valid = false;
            tempErrors.yMax = 'Should be positive';
        }
        if(zMax === '') {
            valid = false;
            tempErrors.zMax = 'This field is required';
        }
        if(xMin === '') {
            valid = false;
            tempErrors.xMin = 'This field is required';
        }
        if(xMin !== '' && xMin < 0) {
            valid = false;
            tempErrors.xMin = 'Should be positive';
        }
        if(yMin === '') {
            valid = false;
            tempErrors.yMin = 'This field is required';
        }
        if(yMin !== '' && yMin < 0) {
            valid = false;
            tempErrors.yMin = 'Should be positive';
        }
        if(zMin === '') {
            valid = false;
            tempErrors.zMin = 'This field is required';
        }
        setErrors(tempErrors);
        if(valid) {
            props.increment('second',{xMax, xMin, yMax, yMin, zMax, zMin});
        }
    };
  
    return (
        <div className={firstStyle.container}>
            <div className={firstStyle.head}>
                <p className={firstStyle.headText}>ABC Company</p>
            </div>
            <div className={firstStyle.form}>
                <div className="d-flex justify-space-between">
                    <div>
                        <div className={secondStyle.inputWrapper}>
                            <label>Project Name</label>
                            <input value={props.name} disabled/>
                        </div>
                        <div className={secondStyle.inputWrapper}>
                            <label>Project Description</label>
                            <textarea
                                rows="8" 
                                value={props.description}
                                disabled
                            />
                        </div>
                        <div className={secondStyle.inputWrapper}>
                            <label>Client</label>
                            <input value={props.client} disabled/>
                        </div>
                        <div className={secondStyle.inputWrapper}>
                            <label>Contractor</label>
                            <input value={props.contractor} disabled/>
                        </div>
                    </div>
                    <div>
                        <div className={firstStyle.inputWrapper}>
                            <label>Upload File(.csv)</label>
                            <CSVReader
                                onDrop={handleOnCsvDrop}
                                config={{ header : true }}
                                addRemoveButton
                            >
                                <span>Drop CSV file here or click to upload.</span>
                            </CSVReader>
                        </div>
                        <div className={firstStyle.inputWrapper}>
                            <div className="d-flex justify-space-between">
                                <div className="mr-1">
                                    <label>X Max Value</label>
                                    <input 
                                        placeholder="Maximum Value"
                                        value={xMax}
                                        type="Number"
                                        disabled={uploadCsv}
                                        onChange={(e) => handlar(e, 'xMax')}
                                    />
                                    {errors.xMax && (
                                        <span className="error">{errors.xMax}</span>
                                    )}
                                </div>
                                <div className="ml-1">
                                    <label>X Min Value</label>
                                    <input 
                                        placeholder="Minimum Value"
                                        value={xMin}
                                        type="Number"
                                        disabled={uploadCsv}
                                        onChange={(e) => handlar(e, 'xMin')}
                                    />
                                    {errors.xMin && (
                                        <span className="error">{errors.xMin}</span>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className={firstStyle.inputWrapper}>
                            <div className="d-flex justify-space-between">
                                <div className="mr-1">
                                    <label>Y Max Value</label>
                                    <input 
                                        placeholder="Maximum Value"
                                        value={yMax}
                                        type="Number"
                                        disabled={uploadCsv}
                                        onChange={(e) => handlar(e, 'yMax')}
                                    />
                                    {errors.yMax && (
                                        <span className="error">{errors.yMax}</span>
                                    )}
                                </div>
                                <div className="ml-1">
                                    <label>Y Min Value</label>
                                    <input 
                                        placeholder="Minimum Value"
                                        value={yMin}
                                        type="Number"
                                        disabled={uploadCsv}
                                        onChange={(e) => handlar(e, 'yMin')}
                                    />
                                    {errors.yMin && (
                                        <span className="error">{errors.yMin}</span>
                                    )}
                                </div>
                            </div>
                        </div>
                        <div className={firstStyle.inputWrapper}>
                            <div className="d-flex justify-space-between">
                                <div className="mr-1">
                                    <label>Z Max Value</label>
                                    <input 
                                        placeholder="Maximum Value"
                                        value={zMax}
                                        type="Number"
                                        disabled={uploadCsv}
                                        onChange={(e) => handlar(e, 'zMax')}
                                    />
                                    {errors.zMax && (
                                        <span className="error">{errors.zMax}</span>
                                    )}
                                </div>
                                <div className="ml-1">
                                    <label>Z Min Value</label>
                                    <input 
                                        placeholder="Minimum Value"
                                        value={zMin}
                                        type="Number"
                                        disabled={uploadCsv}
                                        onChange={(e) => handlar(e, 'zMin')}
                                    />
                                    {errors.zMin && (
                                        <span className="error">{errors.zMin}</span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <button className={firstStyle.btn} onClick={validate}>Next</button>
                </div>
            </div>
        </div>
    );
};

export default First;